
import React, { FC } from 'react'
import { NavBar, TabBar } from 'antd-mobile'
//引入图标
import community3 from "../assets/icons/community-3.png"
import community2 from "../assets/icons/community-2.png"
import homeIcon1 from "../assets/icons/home.png"
import homeIcon2 from "../assets/icons/home-2.png"
import tabBarStyle from "./tabBar.module.css"
import {
    Route,
    useNavigate,
    useLocation,
    MemoryRouter as Router,
} from 'react-router-dom'
import {
    AppOutline,
    MessageOutline,
    UnorderedListOutline,
    UserOutline,
} from 'antd-mobile-icons'
console.log(tabBarStyle)

//自定义图标
let homeIcon = (active: boolean) => {
    // console.log(active)
    let icon = active ? homeIcon2 : homeIcon1
    return <img src={icon} />
}

let communityIcon = (active: boolean) => {
    let icon = active ? community2 : community3
    return <img src={icon} />
}

const Bottom: FC = () => {
    const navigate = useNavigate()
    const location = useLocation()
    const { pathname } = location

    const setRouteActive = (value: string) => {
        navigate(value)
    }

    const tabs = [
        {
            key: '/',
            title: '首页',
            icon: homeIcon,
        },
        {
            key: '/community',
            title: '社区',
            icon: communityIcon,
        },
        {
            key: '/mine',
            title: '我的',
            icon: <UserOutline />,
        },
    ]

    return (
        <>
            <TabBar className={tabBarStyle['adm-tab-bar']} activeKey={pathname} onChange={value => setRouteActive(value)}>
                {tabs.map(item => (
                    <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
                ))}
            </TabBar>
            <h1 id={tabBarStyle.cl} className={[tabBarStyle.ft, tabBarStyle.bd].join(" ")}> 123</h1>
        </>
    )
}

export default Bottom