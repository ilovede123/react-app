
import React from "react"
// import Login from "../pages/login"

let Home = React.lazy(() => import(/*webpackChunkName:"home"*/"../pages/home/index"))

let Login = React.lazy(() => import(/*webpackChunkName:"login"*/"../pages/login/index"))
let Community = React.lazy(() => import(/*webpackChunkName:"community"*/"../pages/community/index"))
let Mine = React.lazy<any>(() => new Promise(resolve => {
    setTimeout(() => {
        resolve(import(/*webpackChunkName:"mine"*/"../pages/mine/index"))
    }, 3000)
}))

//声明类型
type Iroutes = Array<
    {
        path: string,
        element: React.ReactNode,
        children?: Iroutes
    }>


//声明routes配置

let routes = [
    {
        path: "/",
        element: <Home />
    },
    {
        path: "/community",
        element: <Community />
    },
    {
        path: "/mine",
        element: <Mine />
    },
    {
        path: "/login",
        element: <Login />
    }
]

export default routes