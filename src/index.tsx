import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
//引入移动端适配的库
import "lib-flexible/flexible"
//引入路由
import { BrowserRouter as Router } from 'react-router-dom'
//引入工具文件
import './utils'
//引入路由拦截组件
import AuthRouter from './components/authRouter';
//引入Route
import { Routes, Route } from "react-router-dom"
//引入登入页
import Login from './pages/login';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <Router>
    <AuthRouter tokenKey='qf-token'>
      <App />
    </AuthRouter>
  </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
