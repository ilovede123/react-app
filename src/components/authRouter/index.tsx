import React, { ReactNode, FC, Fragment } from "react"
import { Navigate, Routes, Route } from "react-router-dom"
let AuthRouter: FC<{ children?: ReactNode, tokenKey: string }> = ({ children, tokenKey }) => {
    //@ts-ignore
    let token = React.$local.get(tokenKey)
    if (token) {
        return <>{children}</>
    } else {
        return (
            <Fragment>
                {children}
                <Navigate to="/login" />
            </Fragment>
        )
    }

}

export default AuthRouter