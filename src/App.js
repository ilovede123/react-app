

// import "./App.css"
import TabBar from "./layout/tabBar"
import { useLocation, useRoutes } from "react-router-dom"
import routes from "./router/routes"
import { Suspense } from "react"
import { SpinLoading } from "antd-mobile"
import Login from "./pages/login"



function App() {
  let routerElement = useRoutes(routes)
  let location = useLocation()
  return (
    <div className="App">
      <Suspense fallback={<SpinLoading color='primary' />}>
        {routerElement}
      </Suspense>
      {location.pathname !== "/login" ? <TabBar /> : null}
    </div>
  );
}

export default App;
