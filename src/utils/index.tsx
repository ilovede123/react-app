//将模块方法暴露到全局,就是挂载在React对象上

import React from "react"
import * as utils from "./local"
// console.log(utils)
// console.log(Object.keys(utils))

Object.keys(utils).forEach(key => {
    let newKey = '$' + key
    //@ts-ignore 忽略ts类型检查
    React[newKey] = utils[key]
})