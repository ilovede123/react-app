export let local = {
    get(key: string) {
        return localStorage.getItem(key)
    }
}

